### Ferdinand Gregorovius

## Papst Alexander VI. und seine Zeit

Verlag
Die Heimbücherei
Berlin

1942

Druck von Boom-Ruygrok N.V., Haarlem

Buchausstattung von Horst Michel, Berlin

| ![Papst Alexander VI. (Nach dem Gemälde von Pinturicchio) ](https://www.projekt-gutenberg.org/gregorov/papstale/bilder/fronti.jpg}) |
| :--: |
| Papst Alexander VI. (Nach dem Gemälde von Pinturicchio) |

### Zeittafel

1378-1458

Alfonso Borgia (1455 Papst Calixtus III.).

1411-1437

Sigismund, deutscher Kaiser, letzter Sproß aus dem Hause Luxemburg, Sohn Karl IV.; geb. 15.2.1361, gest. 9.12.1437.

1415

Johs. Huss, geb. 1369, wurde am 6. Juli 1415 in Konstanz als Ketzer verbrannt.

1417-1431

Papst Martin V., aus dem Hause der Colonna.

1419-1436

Hussitenkrieg. Ziska, der Anführer der Hussiten, schlägt die Heere Kaiser Sigismunds.

1431-1449

Konzil zu Basel.

1431-1447

Papst Eugen IV., vom Baseler Konzil abgesetzt, wußte sich aber doch zu behaupten.

1431-1503

Rodrigo Borgia (1492 Papst Alexander VI.).

1438-1439

1 Unionskonzil in Ferrara und Florenz. Die Beschlüsse über die Wiedervereinigung des Oström. Reiches (Byzanz) mit Rom werden in Konstantinopel nicht angenommen.

1438-1439

Albrecht II., römischer König, Schwiegersohn Sigismunds.
1440-1493 Friedrich III., der letzte (1452) in Rom gekrönte deutsche Kaiser.

1442

Vanozza de Cantencis, die Geliebte Alexander VI. wird geboren.

1447-1455

Papst Nicolaus V., löste 1449 das Baseler Konzil auf, schloß 1448 mit Friedrich III. das Wiener Konkordat. (Aschaffenburger Konkordat).

1450

Der Condottiere Francesco Sforza wird Herzog von Mailand.

1450

Johann Gutenberg erfindet die Buchdruckerkunst.

1452-1519

Leonardo da Vinci.

1453

Eroberung Konstantinopels durch Sultan Mohamed II. Ende des Oströmischen Reiches.

1455-1458

Papst Calixtus III. (Alfonso Borgia).

1456

Rodrigo Borgia wird Kardinaldiaconus.

1458-1464

Papst Pius II. (Aeneas Sylvius Piccolomini 1405-1464).

1458

Die Türken erobern Athen (1458-1822 unter türk. Herrschaft).

1458

Don Pedro Borgia, der Bruder Rodrigos, stirbt auf der Flucht.

1459-1485

Die Kriege der Roten Rose (Lancaster) mit der Weißen Rose (York) in England.

1464-1471

Papst Paul II.

1471-1484

Papst Sixtus IV. (Francesco Rovere 1414-1484).

1473-1543

Nikolaus Coppernikus, Domherr zu Frauenburg am Frischen Haff, begründet die Lehre von unserem Planetensystem mit der Sonne als Mittelpunkt.

1477-1500

Juan (Johann) Borgia.

1478-1507

Cesare Borgia.

1479-1516

Ferdinand II., der Katholische, vereinigt Kastilien mit Aragon. Erneuerung der Inquisition. 1483 wird der Dominikaner Torquemada zum Großinquisitor ernannt.

1480-1519

Lucrezia Borgia. 1481 Gofreda (José) Borgia.

1481

Mohamed II. stirbt.

1482

Am 24. Januar vermählt sich Girolama Borgia, eine natürliche Tochter des Kardinals Rodrigo Borgia, mit Gian Andrea Cesarini.1483-1546 Martin Luther (1511 Luther in Rom).

1484-1492

Papst Innocenz VIII. (Kardinal Cibo 1432-1492).

1488

Pedro Luis Borgia, Herzog von Gandia (Valencia), der älteste Sohn Alexander VI., stirbt dreißigjährig in Rom. Er setzt seinen Bruder Don Juan (Johann) Borgia zum Erben von Gandia ein.

1491

Lucrezia Borgia wird, zwölfjährig, mit Don Cherubin Juan de Cantalles verlobt.

1492

Lucrezia Borgia wird mit Gasparo von Procida verlobt.

1492-1503

Papst Alexander VI. (Rodrigo Borgia).

1492

Lucrezia Borgia heiratet Giovanni Sforza von Pesaro.

1492

Entdeckung Amerikas durch Columbus. Der starb 1506 in Valladolid, ohne zu wissen, daß er einen neuen Erdteil entdeckt hatte. Erst der Florentiner Amerigo Vespucci konnte Karten und Beschreibungen der neu entdeckten Länder herausbringen. Nach ihm wurde der Erdteil benannt.

1493

Alexander VI. teilt mit einem Federstrich die Welt in zwei Hälften: eine für Spanien, die andere Hälfte für Portugal.

1493-1519

Kaiser Maximilian I., Sohn Friedrich III. Maximilian gelingt die Bildung der Habsburgischen Hausmacht. Durch Heiraten und Verträge gewinnt sein Haus die Herrschaft auch über Spanien, Böhmen und Ungarn. Maximilian führte das »Römische Recht« in Deutschland ein.

1494

Karl VIII. von Frankreich (1483-1498) zieht nach Italien, die Ansprüche des Hauses Anjou auf das Königreich Italien geltend machend. Obwohl er Neapel erobert, wird er doch zum Rückzuge genötigt durch das Bündnis, das Alexander VI. mit Maximilian und dem Herzog von Mailand, sowie mit Venedig und Spanien schließt.

1495

Erstes Auftreten der Lues in Italien (Europa).

1497

Juan Borgia, Kardinal von Santa Susanna, Herzog von Gandia, wird am 14. Juni 1497 von seinem Bruder Cesare Borgia ermordet.

1497

wird die kinderlose Ehe der Lucrezia Borgia mit Giovanni Sforza von Pesaro von Alexander VI. geschieden.

1498

Lucrezia Borgia heiratet Don Alfonso von Bisceglie am 20. Juli 1498.

1498

Savonarola wird am 23. Mai 1498 in Florenz auf dem Scheiterhaufen als Ketzer verbrannt, weil er gegen die Unsittlichkeit der Borgias aufgetreten war.

1498

Vasco da Gama entdeckt den Seeweg nach Ostindien.

1500

Johann Borgia, Kardinal-Legat, wird am 14. Jan. 1500 von Cesare Borgia vergiftet.

1500

Alfonso, Herzog von Bisceglie und Prinz von Salerno, der Gatte der Lucrezia Borgia, wird von Cesare Borgia am 15. Juli 1500 ermordet.

1501

Ludwig XII. von Frankreich im Bunde mit Ferdinand dem Katholischen, König von Aragon, erobert das Königreich Neapel. Jedoch werden die Franzosen und Spanier bald uneinig. Ludwig der XII. verzichtet auf Neapel.

1501

Lucrezia Borgia heiratet am 28. Dez. 1501 Alfonso von Este, wobei Fernandes von Este seinen abwesenden Bruder vertrat.

1503

Papst Alexander VI. stirbt am 18. August, 72 Jahre alt.

1503

Papst Pius III. wird am 22. Sept. gewählt, am 8. Oktober gekrönt. Er stirbt am 18. Oktober.

1503

Papst Julius II. wird am 2. Nov. gewählt.

1507

Cesare Borgia fällt am 12. März in der Schlacht bei Viana in Spanien.

1519

Lucrezia Borgia stirbt.

* * *

Anmerkungen als Fußnoten eingearbeitet. joe_ebc für Gutenberg.
